from django.urls import path
from projects.views import projectlist, showproject, createproject


urlpatterns = [
    path("", projectlist, name="list_projects"),
    path("<int:id>/", showproject, name="show_project"),
    path("create/", createproject, name="create_project"),
]
