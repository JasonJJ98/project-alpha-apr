from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from projects.forms import ProjectForm
from django.contrib.auth.decorators import login_required


@login_required
def projectlist(request):
    project = Project.objects.filter(owner=request.user)
    context = {
        "projects": project,
    }
    return render(request, "projects/list.html", context)


@login_required
def showproject(request, id):
    project = get_object_or_404(Project, id=id)
    context = {
        "projects": project,
    }
    return render(request, "projects/showproject.html", context)


@login_required
def createproject(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/createproject.html", context)
