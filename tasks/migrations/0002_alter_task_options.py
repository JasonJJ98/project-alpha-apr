# Generated by Django 4.1.7 on 2023-03-07 17:42

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("tasks", "0001_initial"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="task",
            options={"ordering": ["-is_completed", "name"]},
        ),
    ]
