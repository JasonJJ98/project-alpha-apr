from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required


@login_required
def createtask(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/createtask.html", context)


@login_required
def tasklist(request):
    task = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": task,
    }
    return render(request, "tasks/tasklist.html", context)
