from django.urls import path
from tasks.views import createtask, tasklist


urlpatterns = [
    path("create/", createtask, name="create_task"),
    path("mine/", tasklist, name="show_my_tasks"),
]
